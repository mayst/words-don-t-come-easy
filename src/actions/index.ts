import addPlayer from './addPlayer';

export default function() {
  this.action('addPlayer', addPlayer);
}
