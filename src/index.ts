'use strict';

import { config } from 'dotenv';

config();

import Telegraf from 'telegraf';
import * as session from 'telegraf/session';
import * as Markup from 'telegraf/markup';
import actions from './actions';
// const RedisSession = require('telegraf-session-redis')
// const SessionCheck = require('./middlewares/SessionCheck');
const telegraf = new Telegraf(process.env.TELEGRAM_KEY || '');


// const session = new RedisSession({
//   store: {
//     host: process.env.TELEGRAM_SESSION_HOST || '127.0.0.1',
//     port: process.env.TELEGRAM_SESSION_PORT || 6379
//   },
// property: 'test'
// })

try {
  telegraf.use(session());

// const db = require('./helpers/db');
// add all scenes
// const stage = require('./scenes')(telegraf);

// db()

// telegraf.use(stage.middleware())
// telegraf.use(SessionCheck)

  actions.call(telegraf);

  telegraf.catch((err, ctx) => {
    console.log(`Ooops, encountered an error for ${ctx.updateType}`, err);
  });

  telegraf.start(async (ctx: any) => {
    const menu = Markup
      .inlineKeyboard([
        Markup.callbackButton('+', 'addPlayer')
      ])
      .extra();

    await ctx.reply(`Game started, to join the players list, press the button 'Join'!`, menu);
  });

  telegraf.launch();

  console.log('i`m working');
} catch(e) {
  console.log(e);
}